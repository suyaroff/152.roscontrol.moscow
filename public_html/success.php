<?

include('config.php');

if (count($_POST)) {
    $_POST = array_map('trim', $_POST);
    /*
    $data = array(
        'inn' => iconv('windows-1251', 'utf-8', $_POST['inn']),
        'source' => iconv('windows-1251', 'utf-8', $_POST['source']),
        'company_name' => iconv('windows-1251', 'utf-8', $_POST['company_name']),
        'director_name' => iconv('windows-1251', 'utf-8', $_POST['director_name']),
        'email' => iconv('windows-1251', 'utf-8', $_POST['email']),
        'phone' => iconv('windows-1251', 'utf-8', $_POST['phone']),
    );
    */
    $data = array(
        'inn' => $_POST['inn'],
        'source' => $_POST['source'],
        'company_name' => $_POST['company_name'],
        'director_name' => $_POST['director_name'],
        'email' => $_POST['email'],
        'phone' => $_POST['phone'],
    );


    /* сохраняем в базу */
    $sql = "INSERT INTO emal_company ( inn, source, company_name, director_name , email, phone) VALUES 
           (:inn, :source, :company_name, :director_name , :email, :phone)";
    $sth = $dbh->prepare($sql);
    $sth->execute(array(
        ':inn' => $data['inn'],
        ':source' => $data['source'],
        ':company_name' => $data['company_name'],
        ':director_name' => $data['director_name'],
        ':email' => $data['email'],
        ':phone' => $data['phone'],
    ));

    /* составляем письмо и отправляем */
    $patterns = array(
        '/\{inn\}/',
        '/\{source\}/',
        '/\{company_name\}/',
        '/\{director_name\}/',
        '/\{email\}/',
        '/\{phone\}/',
    );
    $message = file_get_contents('notification_email.html');
    $mail['message'] = preg_replace($patterns, array_values($data), $message);

    $mail['to'] = 'landingaggregator@gmail.com'; // кому
    $mail['from'] = 'landingaggregator@gmail.com'; // от кого
    $mail['subject'] = 'Заявка с рассылки';

    send($mail);
}


?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="index,follow">
    <meta name="keywords" content="ЗПД, защита персональных данных, 152 ФЗ">
    <meta name="description" content="Получить сертификат соответствия и документацию по защите персональных данных по субсидии. Работа по всей России">
    <title>Федеральная программа защиты персональных данных по 152-ФЗ</title>

    <link rel="stylesheet" type="text/css" href="assets/slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/slick-1.8.1/slick/slick-theme.css">

    <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=cyrillic" rel="stylesheet">

    <script src="assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="assets/slick-1.8.1/slick/slick.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.scrollTo.min.js" type="text/javascript"></script>
    <script src="assets/js/script.js?v=0.1" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

    <link rel="shortcut icon" href="/img/favicon.png" type="image/png">

</head>
<body>

<div class="wrapper">
    <header class="header">
        <a href="/" class="logo">
            <img src="assets/img/logo.png">
            <div class="logo-text">
                <b>РОССЕРТ</b>
                <small>Единый центр сертификации</small>
            </div>
        </a>

        <div class="side">
            <div class="info">
                <b class="tyellow">Федеральная программа сертификации<b class="tred">*</b>.</b><br>
                На основе СДС «Россертификация»<br>
                зарегистрированной в Росстандарте.
            </div>
        </div>

    </header>
    <section id="section_1">
        <div class="container">
            <h1>
                <span>Федеральная программа по защите</span><br>
                <span>персональных данных - 2018</span>
            </h1>
            <div class="description">
                C 25 июля 2011г. <strong>все действующие организации Российской Федерации,</strong><br>
                <strong>обязаны подтвердить</strong> соответствие Федеральному закону № 152-ФЗ от<br>
                27.07.2006г. "О персональных данных" (в ред. от 03.07.2016), а именно,<br>
                разработать и внедрить <strong>систему защиты персональных данных.</strong>
            </div>
        </div>
        <ul class="bottom">
            <li>Работаем по всей<br> России</li>
            <li>Все документы подлинны,<br>занесены в единый регистр</li>
            <li>Доступная ценовая политика<br> скидки, рассрочки</li>
        </ul>
    </section>

    <section id="section_6">
        <br>
        <br>
        <br>
        <br>
        <h2 class="section-title"><span>Если вы оставили корректный номер телефона,<br> то специалист свяжется с вами в порядке очереди.</span></h2>
        <div class="tc fs-16">
Спасибо!
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>

    </section>

</div>

<footer class="footer">
    © 2018 г., Официальный Интернет-ресурс СДС «Россертификация». Все права на материалы, охраняются в соответствии с законодательством Российской Федерации.<br>
    <a href="assets/docs/Politica_ecs.pdf"><strong>Политика в отношении обработки персональных данных</strong></a>
</footer>

<script>
    setTimeout(function () {
        location = 'http://xn--152--84d1f.xn--e1arfcdaj.xn--p1ai/';
    }, 7000)
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter42526294 = new Ya.Metrika2({
                    id: 42526294,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/42526294" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-99163604-1', 'auto');
    ga('send', 'pageview');

</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/42526294" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->


</body>
</html>