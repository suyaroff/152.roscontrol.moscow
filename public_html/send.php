<?php
/**
 * Created by PhpStorm.
 * User: jamal Suyarov
 * Date: 08.06.2018
 * Time: 18:23
 */

header('Content-Type: text/html; charset=utf-8', true);

function send($data)
{
    $subject = '=?utf-8?b?' . base64_encode($data['subject']) . '?=';
    $headers = "Content-type: text/html; charset=\"utf-8\"\r\n";
    $headers .= "From: <" . $data['from'] . ">\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Date: " . date('D, d M Y h:i:s O') . "\r\n";

    if (mail($data['to'], $subject, $data['message'], $headers, '-f' . $data['from'])) {
        return true;
    } else {
        return false;
    }
}


if (isset($_POST['phone'])) {
    $message = file_get_contents('email_template/email.html');
    $data = array(
        'phone' => $_POST['phone'],
        'email' => $_POST['email'],
        'inn' => $_POST['inn'],
    );
    $patterns = array(
        '/\{phone\}/',
        '/\{email\}/',
        '/\{inn\}/',
    );

    $mail['message'] = preg_replace($patterns, array_values($data), $message);

    $mail['to'] = 'landingaggregator@gmail.com'; //куда

    $mail['subject'] = 'Заявка';
    $mail['from'] = $data['email'];
    if (send($mail)) {
        /* отправляем письмо заказчику */
        $client_mail = [
            'from' => 'landingaggregator@gmail.com', // от кого
            'to' => $data['email'],
            'subject' => 'Красивое письмо',
            'message' => file_get_contents('email_template/client.html')
        ];
        send($client_mail);

        /* обновляем количество отправленых заявок */
        $orders = file_get_contents('order_count.txt');
        settype($orders, 'int');
        $orders++;
        file_put_contents('order_count.txt', $orders);

        echo 'success';
    } else {
        echo 'error';
    }

}

