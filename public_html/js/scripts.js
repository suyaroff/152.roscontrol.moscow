function sendOrder() {
    var order = $('#order input').serializeArray();

    if (order[0].value == '') {
        alert('Введите номер телефона');
        return false;
    }
    if (order[1].value == '') {
        alert('Введите email');
        return false;
    }

    $.post('/send.php', order, function (r) {
        if (r == 'success') {
            $('#order').html('<div id="success">Ваша заявка отправлена</div>');
            setTimeout(hideOverlay, 2000);
        }
        if (r == 'error') {
            alert('Ошибка при отправлении email');
        }
    });

}

function hideOverlay() {
    $('#overlay').hide();
}

function showOverlay() {
    $('#overlay').css('display', 'flex');
}

function showForm() {
    showOverlay();
}

var month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

var date = new Date();
var currentMonth = month[date.getMonth()];

$(document).ready(function () {
    $('#currentMon').html(currentMonth);
    var campaign = window.location.href.match(/utm_campaign=(.*)-(\d*)/);

    if (campaign) {
        $('input[name="email"]').val(campaign[1]);
        $('input[name="inn"]').val(campaign[2]);

    }

    $("#phone").mask("+7(999)999-9999");
    $('#overlay').on('click', function (e) {
        if (e.target.id == 'overlay') {
            hideOverlay();
        }
    });

    $.get('/order_count.txt?v='+Math.random(), function (r) {
        $('#orderCount').html(r);
    })


});


